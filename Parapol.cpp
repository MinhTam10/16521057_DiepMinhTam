#include "Parapol.h"

void Draw2Points(int xc, int yc, int x, int y, SDL_Renderer *ren)
{
    SDL_RenderDrawPoint(ren, xc+x, yc+ y);
	SDL_RenderDrawPoint(ren, xc-x, yc+ y);
}
void BresenhamDrawParapolPositive(int xc, int yc, int A, SDL_Renderer *ren)
{
	//first area :))))
	int p=1-A;
	Draw2Points(xc, yc, 0,0,ren);
	int x = 0, y = 0;
	while(x <= A){
		if( p <=0 )
			p+=2*x+3;
		else{
			p+=2*x+3-2*A;
			y+=1;
		}
		x+=1;
		Draw2Points(xc, yc, x, y,ren);
	}
	p=2*A -1;

	Draw2Points(xc, yc, A, A/2,ren);
	x=A; y=A/2;
	while(y < 600){
	    if(p <= 0 )
	    	p+=4*A;
    	else{
		    p+=4*A-4*x-4;
	    	x+=1;
    	}
    	y+=1;
    	Draw2Points(xc, yc, x, y,ren);
	}
}

void BresenhamDrawParapolNegative(int xc, int yc, int A, SDL_Renderer *ren)
{
		//A<0 -> A =-A 
	if(A<0)
		A=-A;
		// do the samething as POSITIVE and DRAWbackward
		int p=1-A;
	Draw2Points(xc, yc, 0,0,ren);
	int x = 0, y = 0;
	while(x <= A){
		if( p <=0 )
			p+=2*x+3;
		else{	
			p+=2*x+3-2*A;
		y+=1;
		}
		x+=1;
		Draw2Points(xc, yc, x, -y,ren);
	}
	p=2*A -1;
	x=A; y=A/2;
	Draw2Points(xc, yc, A, -A/2,ren);
	while(y < 600){
	    if(p <= 0 )
	    	p+=4*A;
    	else{	
	    	p+=4*A-4*x-4;
    	x+=1;
    	}
    	y+=1;
    	Draw2Points(xc, yc, x, -y,ren);
	}
}
