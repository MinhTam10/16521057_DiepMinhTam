//#include "stdafx.h"
#include "FillColor.h"
#include <iostream>
#include <stack>
#include <cmath>
using namespace std;

//Get color of a pixel
SDL_Color getPixelColor(Uint32 pixel_format, Uint32 pixel)
{
	SDL_PixelFormat* fmt = SDL_AllocFormat(pixel_format);

	Uint32 temp;
	Uint8 red, green, blue, alpha;

	/* Get Red component */
	temp = pixel & fmt->Rmask;  /* Isolate red component */
	temp = temp >> fmt->Rshift; /* Shift it down to 8-bit */
	temp = temp << fmt->Rloss;  /* Expand to a full 8-bit number */
	red = (Uint8)temp;

	/* Get Green component */
	temp = pixel & fmt->Gmask;  /* Isolate green component */
	temp = temp >> fmt->Gshift; /* Shift it down to 8-bit */
	temp = temp << fmt->Gloss;  /* Expand to a full 8-bit number */
	green = (Uint8)temp;

	/* Get Blue component */
	temp = pixel & fmt->Bmask;  /* Isolate blue component */
	temp = temp >> fmt->Bshift; /* Shift it down to 8-bit */
	temp = temp << fmt->Bloss;  /* Expand to a full 8-bit number */
	blue = (Uint8)temp;

	/* Get Alpha component */
	temp = pixel & fmt->Amask;  /* Isolate alpha component */
	temp = temp >> fmt->Ashift; /* Shift it down to 8-bit */
	temp = temp << fmt->Aloss;  /* Expand to a full 8-bit number */
	alpha = (Uint8)temp;

	SDL_Color color = { red, green, blue, alpha };
	return color;

}

//Get all pixels on the window
SDL_Surface* getPixels(SDL_Window* SDLWindow, SDL_Renderer* SDLRenderer) {
	SDL_Surface* saveSurface = NULL;
	SDL_Surface* infoSurface = NULL;
	infoSurface = SDL_GetWindowSurface(SDLWindow);
	if (infoSurface == NULL) {
		std::cerr << "Failed to create info surface from window in saveScreenshotBMP(string), SDL_GetError() - " << SDL_GetError() << "\n";
	}
	else {
		unsigned char * pixels = new (std::nothrow) unsigned char[infoSurface->w * infoSurface->h * infoSurface->format->BytesPerPixel];
		if (pixels == 0) {
			std::cerr << "Unable to allocate memory for screenshot pixel data buffer!\n";
			return NULL;
		}
		else {
			if (SDL_RenderReadPixels(SDLRenderer, &infoSurface->clip_rect, infoSurface->format->format, pixels, infoSurface->w * infoSurface->format->BytesPerPixel) != 0) {
				std::cerr << "Failed to read pixel data from SDL_Renderer object. SDL_GetError() - " << SDL_GetError() << "\n";
				delete[] pixels;
				return NULL;
			}
			else {
				saveSurface = SDL_CreateRGBSurfaceFrom(pixels, infoSurface->w, infoSurface->h, infoSurface->format->BitsPerPixel, infoSurface->w * infoSurface->format->BytesPerPixel, infoSurface->format->Rmask, infoSurface->format->Gmask, infoSurface->format->Bmask, infoSurface->format->Amask);
			}
			delete[] pixels;
		}
	}
	return infoSurface;
}

//Compare two colors
Uint32 get_pixel32(SDL_Surface *surface, int x, int y)
{
	//Convert the pixels to 32 bit
	Uint32 *pixels = (Uint32 *)surface->pixels;

	//Get the requested pixel
	return pixels[(y * surface->w) + x];
}
bool compareTwoColors(SDL_Color color1, SDL_Color color2)
{
	if (color1.r == color2.r && color1.g == color2.g && color1.b == color2.b && color1.a == color2.a)
		return true;
	return false;
}

void BoundaryFill4(SDL_Window *win, Vector2D startPoint, Uint32 pixel_format,
	SDL_Renderer *ren, SDL_Color fillColor, SDL_Color boundaryColor)
{
	SDL_SetRenderDrawColor(ren, fillColor.r, fillColor.g, fillColor.b, fillColor.a);
	stack <pair<int, int>> B;
	int max_y;
	int max_x; 
	SDL_GetWindowSize(win, &max_x, &max_y);
	
	SDL_Surface *A = getPixels(win, ren);

	B.push(make_pair(startPoint.x, startPoint.y));
	while (!B.empty()) {  
		int x = B.top().first;
		int y = B.top().second;
	//	cout << "xet: " << x << " " << y << endl;
		SDL_Surface *A = getPixels(win, ren);
		Uint32 pixel = get_pixel32(A, x, y);
		SDL_Color color1 = getPixelColor(pixel_format, pixel);
		color1.a = 255;

		if ((!compareTwoColors(color1, boundaryColor)) &&  (!compareTwoColors(color1, fillColor))) {
			SDL_RenderDrawPoint(ren, x, y);
		//	cout << "To: " << x << " " << y << endl;
		}
		pixel = get_pixel32(A, x - 1, y);
		color1 = getPixelColor(pixel_format, pixel);
		color1.a = 255;
		if ((!compareTwoColors(color1, boundaryColor)) &&(x-1> 0)&&(y>=0)&& (x-1 <max_x)&&(y < max_y)&& (!compareTwoColors(color1, fillColor))) {
			B.push(make_pair(x - 1, y));
		//	cout << "Them: " << x - 1 << " " << y << endl;
			continue;
		}
		pixel = get_pixel32(A, x + 1, y);
		color1 = getPixelColor(pixel_format, pixel);
		color1.a = 255;
		if ((!compareTwoColors(color1, boundaryColor)) &&(x+1>=0) && (y>=0)&& (x+1 < max_x )&& (y<max_y)&& (!compareTwoColors(color1, fillColor))) {
			B.push(make_pair(x + 1, y));
		//	cout << "Them: " << x + 1 << " " << y << endl;
			continue;
		}
		pixel = get_pixel32(A, x, y + 1);
		color1 = getPixelColor(pixel_format, pixel);
		color1.a = 255;
		if ((!compareTwoColors(color1, boundaryColor)) &&(x>=0)&&(y+1>=0)  && ( x < max_x) && (y+1 <max_y) &&(!compareTwoColors(color1, fillColor))) {
			B.push(make_pair(x, y + 1));
		//	cout << "Them: " << x << " " << y + 1 << endl;
			continue;
		}
		pixel = get_pixel32(A, x, y - 1);
		color1 = getPixelColor(pixel_format, pixel);
		color1.a = 255;
		if ((!compareTwoColors(color1, boundaryColor)) &&(x>=0) && (y-1 >=0) &&(x <max_x) && (y-1 < max_y) && (!compareTwoColors(color1, fillColor))) {
			B.pop();
			B.push(make_pair(x, y - 1));
		//	cout << "Them: " << x << " " << y - 1 << endl;
			
			continue;
		}
			
			
	B.pop();
	
		}
//	cout << (B.empty()) << endl;
	//cout << "done" << endl;
}
	/*if ((!compareTwoColors(color1, boundaryColor)) && (!compareTwoColors(color1, fillColor))) {
	SDL_RenderDrawPoint(ren, startPoint.x, startPoint.y);

	Vector2D e(startPoint.x - 1, startPoint.y);
	BoundaryFill4(win, e, pixel_format, ren, fillColor, boundaryColor);
	Vector2D f(startPoint.x + 1, startPoint.y);
	BoundaryFill4(win, f, pixel_format, ren, fillColor, boundaryColor);
	Vector2D g(startPoint.x, startPoint.y - 1);
	BoundaryFill4(win, g, pixel_format, ren, fillColor, boundaryColor);
	Vector2D h(startPoint.x, startPoint.y + 1);
	BoundaryFill4(win,h, pixel_format, ren, fillColor, boundaryColor);
	}*/




//======================================================================================================================
//=============================================FILLING TRIANGLE=========================================================

int maxIn3(int a, int b, int c)
{
	if (a >= b && a >=c)
			return a;
	if (b >= a && b >= c)
		return b;
	if (c >= a && c >= b)
		return c;
	
}

int minIn3(int a, int b, int c)
{
	if (a <= b && a <= c)
		return a;
	if (b <= a && b <= c)
		return b;
	if (c <= a && c <= b)
		return c;
}

void swap(Vector2D &a, Vector2D &b)
{
	Vector2D temp;
	temp = a;
	a = b;
	b = temp;
}

void ascendingSort(Vector2D &v1, Vector2D &v2, Vector2D &v3)
{
	if (v1.y > v2.y)
		swap(v1, v2);
	if (v1.y > v3.y)
		swap(v1, v3);
	if (v2.y > v3.y)
		swap(v2, v3);
}

void TriangleFill1(Vector2D v1, Vector2D v2, Vector2D v3, SDL_Renderer *ren, SDL_Color fillColor)
{
	int x_min = minIn3(v1.x, v2.x, v3.x);
	int x_max = maxIn3(v1.x, v2.x, v3.x);
	DDA_Line(x_min, v1.y, x_max, v1.y, ren);
}

void TriangleFill2(Vector2D v1, Vector2D v2, Vector2D v3, SDL_Renderer *ren, SDL_Color fillColor)
{
	float const_1 = (float)(v3.x - v1.x) / (v3.y - v1.y),
		const_2 = (float)(v3.x - v2.x) / (v3.y - v2.y),
		x_Left = v1.x,
		x_Right = v2.x;
	for (int i = v1.y; i < v3.y; i++) {
		x_Left += const_1;
		x_Right += const_2;
		DDA_Line(int(x_Left + 0.5), i, int(x_Right + 0.5), i, ren);
	}
}

void TriangleFill3(Vector2D v1, Vector2D v2, Vector2D v3, SDL_Renderer *ren, SDL_Color fillColor)
{
	float const_1 = (float)(v2.x - v1.x) / (v2.y - v1.y),
		const_2 = (float)(v3.x - v1.x) / (v3.y - v1.y),
		x_Left = v1.x,
		x_Right = v1.x;
	for (int i = v1.y; i <= v3.y; i++) {
		x_Left += const_1;
		x_Right += const_2;
		DDA_Line(int(x_Left + 0.5), i, int(x_Right + 0.5), i, ren);

	}

}

void TriangleFill4(Vector2D v1, Vector2D v2, Vector2D v3, SDL_Renderer *ren, SDL_Color fillColor)
{
	float const_1 = (float)(v3.x - v1.x) / (v3.y - v1.y);
	float x_mid = v1.x;
	for (int i = v1.y; i <= v2.y; i++)
		x_mid += const_1;
	Vector2D v4(int(x_mid + 0.5), v2.y);
	TriangleFill2(v2, v4, v3, ren, fillColor);
	TriangleFill3(v1, v2, v4, ren, fillColor);
}

void TriangleFill(Vector2D v1, Vector2D v2, Vector2D v3, SDL_Renderer *ren, SDL_Color fillColor)
{
	ascendingSort(v1, v2, v3);
	if (v1.y == v2.y && v2.y == v3.y)
		TriangleFill1(v1, v2, v3, ren, fillColor);
	if (v1.y == v2.y && v3.y > v2.y)
		TriangleFill2(v1,v2,v3,ren,fillColor);
	if (v1.y < v2.y && v2.y == v3.y)
		TriangleFill3(v1, v2, v3, ren, fillColor);
	if (v1.y < v2.y && v2.y < v3.y)
		TriangleFill4(v1, v2, v3, ren, fillColor);
}

bool isInsideCircle(int xc, int yc, int R, int x, int y)
{
	int dx = x - xc;
	int dy = y - yc;
	if (R*R < dx*dx + dy * dy) {
		cout << R << " " << dx * dx + dy * dy << endl;
		return false;
	}
	return true;
}

void FillIntersection(int x1, int y1, int x2, int y2, int xc, int yc, int R,
	SDL_Renderer *ren, SDL_Color fillColor) //duong thang nam ngang
{
	int min_x, max_x;
	if (x1 > x2) {
		min_x = x2;
		max_x = x1;
	}
	else {
		min_x = x1;
		max_x = x2;
	}
	for (int x = min_x; x <= max_x; x++) {
		if (isInsideCircle(xc, yc, R, x, y1)) {
			SDL_SetRenderDrawColor(ren, fillColor.r, fillColor.g+1, fillColor.b, fillColor.a);
			SDL_RenderDrawPoint(ren, x, y1);
			cout << "draw" << endl;
		}
	}
}

void FillIntersectionRectangleCircle(Vector2D vTopLeft, Vector2D vBottomRight, int xc, int yc, int R,
	SDL_Renderer *ren, SDL_Color fillColor)
{
	int minY, maxY, minX, maxX;
	if (vTopLeft.y > vBottomRight.y) {
		minY = vBottomRight.y;
		maxY = vTopLeft.y;
	}
	else {
		minY = vTopLeft.y;
		maxY = vBottomRight.y;
	}
	if (vTopLeft.x > vBottomRight.x) {
		minX = vBottomRight.x;
		maxX = vTopLeft.x;
	}
	else {
		minX = vTopLeft.x;
		maxX = vBottomRight.x;
	}
	for (int y = minY; y <= maxY; y++) {
		for (int x = minX; x <= maxX; x++) {
			if (isInsideCircle(xc, yc, R, x, y))
				SDL_RenderDrawPoint(ren, x, y);
		}
	}
}

void RectangleFill(Vector2D vTopLeft, Vector2D vBottomRight, SDL_Renderer *ren, SDL_Color fillColor)
{	
	int minY, maxY;
	if (vTopLeft.y > vBottomRight.y) {
		minY = vBottomRight.y;
		maxY = vTopLeft.y;
	}
	else {
		minY = vTopLeft.y;
		maxY = vBottomRight.y;
	}
	for (int y = minY; y <= maxY; y++)
		DDA_Line(vBottomRight.x, y, vTopLeft.x, y,ren);
}

void put4line(int xc, int yc, int x, int y, SDL_Renderer *ren, SDL_Color fillColor)
{
	DDA_Line(xc + x, yc + y, xc - x, yc + y, ren);
	DDA_Line(xc + y, yc + x, xc - y, yc + x, ren);
	DDA_Line(xc + x, yc - y, xc - x, yc - y, ren);
	DDA_Line(xc + y, yc - x, xc - y, yc - x, ren);
}

void CircleFill(int xc, int yc, int R, SDL_Renderer *ren, SDL_Color fillColor)
{
	int x = R, y = 0;
	int p = 1 - R;
	put4line(xc, yc, x, y, ren,fillColor);
	while (x>y) {
		if (p <= 0)
			p += 2 * y + 3;
		else {
			p += 2 * y - 2 * x + 5;
			x -= 1;
		}
		y += 1;
		put4line(xc, yc, x, y, ren,fillColor);
	}
}
bool isInsideEllipse(int x, int y, int xce, int yce, int a, int b) {
	int dx = x - xce;
	int dy = y - yce;
	if (dx*dx*b*b + dy*dy*a*a <= a*a*b*b)
		return true;
	return false;
}
void FillIntersectionEllipse_Line(int x1, int y1, int x2, int y2, int xce, int yce, int a, int b, SDL_Renderer *ren, SDL_Color fillColor) {
	int min_x, max_x;
	if (x1 > x2) {
		min_x = x2;
		max_x = x1;
	}
	else {
		min_x = x1;
		max_x = x2;
	}
	SDL_SetRenderDrawColor(ren, fillColor.r, fillColor.g, fillColor.b, fillColor.a);
	for (int x = min_x; x <= max_x; x++) {
		if (isInsideEllipse(x, y1, xce, yce, a, b)) {
			SDL_SetRenderDrawColor(ren, fillColor.r, fillColor.g + 1, fillColor.b, fillColor.a);
			SDL_RenderDrawPoint(ren, x, y1);
			cout << "draw" << endl;
		}

	}
}
void put4lineCircle_Ellipse(int x, int y, int xc1, int yc1, int xce, int yce, int a, int b, SDL_Renderer *ren, SDL_Color fillColor) {
	FillIntersectionEllipse_Line(xc1 + x, yc1 + y, xc1 - x, yc1 + y, xce, yce, a,b, ren, fillColor);
	FillIntersectionEllipse_Line(xc1 + y, yc1 + x, xc1 - y, yc1 + x, xce, yce, a, b, ren, fillColor);
	FillIntersectionEllipse_Line(xc1 + x, yc1 - y, xc1 - x, yc1 - y, xce, yce, a, b, ren, fillColor);
	FillIntersectionEllipse_Line(xc1 + y, yc1 - x, xc1 - y, yc1 - x, xce, yce, a, b, ren, fillColor);
}
void FillIntersectionEllipseCircle(int xcE, int ycE, int a, int b, int xc, int yc, int R,
	SDL_Renderer *ren, SDL_Color fillColor)

{
	int x = R, y = 0;
	int p = 1 - R;
	put4lineCircle_Ellipse(x, y, xc, yc,xcE, ycE, a, b , ren , fillColor);
	while (x>y) {
		if (p <= 0)
			p += 2 * y + 3;
		else {
			p += 2 * y - 2 * x + 5;
			x -= 1;
		}
		y += 1;
		put4lineCircle_Ellipse(x, y, xc, yc, xcE, ycE, a, b, ren, fillColor);
	}

}
void put4line2circle(int x, int y, int xc1, int yc1, int xc2, int yc2, int R2,
	SDL_Renderer *ren, SDL_Color fillColor) {
	FillIntersection(xc1 + x, yc1 + y, xc1 - x, yc1 + y, xc2, yc2,R2, ren, fillColor);
	FillIntersection(xc1 + y, yc1 + x, xc1 - y, yc1 + x, xc2, yc2, R2, ren, fillColor);
	FillIntersection(xc1 + x, yc1 - y, xc1 - x, yc1 - y, xc2, yc2, R2, ren, fillColor);
	FillIntersection(xc1 + y, yc1 - x, xc1 - y, yc1 - x, xc2, yc2, R2, ren, fillColor);
}
void FillIntersectionTwoCircles(int xc1, int yc1, int R1, int xc2, int yc2, int R2,
	SDL_Renderer *ren, SDL_Color fillColor)
{
	cout << "going" << endl;
	int x = R1, y = 0;
	int p = 1 - R1;
	put4line2circle(x,y,xc1, yc1, xc2, yc2,R2, ren, fillColor);
	while (x>y) {
		if (p <= 0)
			p += 2 * y + 3;
		else {
			p += 2 * y - 2 * x + 5;
			x -= 1;
		}
		y += 1;
		put4line2circle(x, y, xc1, yc1, xc2, yc2, R2, ren, fillColor);
	}
}
